#A heurística pensada para este problema foi baseada no
#movimento alternado entre os amigos. Ou seja, cada amigo
#movimenta-se em direção ao outro, tendo cada um a sua vez,
#até que se encontrem no ponto médio entre eles.

def main():
	friend1_pos = int(input())
	friend2_pos = int(input())

	f1_exhaustion = 0
	f2_exhaustion = 0

	exhaustion_iterator = 1

#	Se o amigo 1 se encontra à "esquerda" do amigo 2, eles se
#movimentarão para a "direita" e "esquerda" respectivamente.
	if(friend1_pos < friend2_pos):
		while(friend1_pos != friend2_pos):
			friend1_pos += 1
			f1_exhaustion += exhaustion_iterator

			if(friend1_pos == friend2_pos):
				return f1_exhaustion + f2_exhaustion

			friend2_pos -= 1
			f2_exhaustion += exhaustion_iterator

			if(friend1_pos == friend2_pos):
				return f1_exhaustion + f2_exhaustion

			exhaustion_iterator += 1

#Caso contrário, os sentidos são alternados.
	else:
		while(friend1_pos != friend2_pos):
			friend1_pos -= 1
			f1_exhaustion += exhaustion_iterator

			if(friend1_pos == friend2_pos):
				return f1_exhaustion + f2_exhaustion

			friend2_pos += 1
			f2_exhaustion += exhaustion_iterator
			if(friend1_pos == friend2_pos):
				return f1_exhaustion + f2_exhaustion

			exhaustion_iterator += 1

print(main())