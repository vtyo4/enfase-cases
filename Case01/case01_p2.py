#A heurística pensada para este problema foi baseada em
#ler o jogo da velha na forma de um vetor. A validez do
#jogo é baseada na quantidade de peças que cada jogador
#tem no jogo e se essas peças se encontram em posições
#de vitória.

winning_positions = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[2, 4, 6]
]

#Separa uma string em seus caracteres
def split_str(string):
	return [char for char in string]

#Diz a quantidade de uma determinada peça no jogo
def quantity(player_piece, ttt_grid):
	i = 0
	for x in ttt_grid:
		if(x == player_piece):
				i += 1

	return i

#Checa, se o jogador com tal peça, venceu o jogo
#pelas posições de vitória
def check_winner(player_piece, ttt_grid):
	for x in winning_positions:
		if(ttt_grid[x[0]] == player_piece and
				(ttt_grid[x[0]] == ttt_grid[x[1]] == ttt_grid[x[2]])
			):
				return True
	return False

def main():	
	ttt_grid = []

	for x in range(3):	
		ttt_grid.extend(split_str(raw_input()))

	qt_x = quantity("X", ttt_grid)
	qt_0 = quantity("0", ttt_grid)

	if(qt_x > qt_0 + 1 or qt_0 > qt_x):
		print("invalido")
		return

	winner_x = check_winner("X", ttt_grid)
	winner_0 = check_winner("0", ttt_grid)

	if(winner_x and winner_0):
		print("invalido")
	elif(winner_x and qt_x != qt_0 + 1):
		print("invalido")
	elif(winner_0 and qt_x != qt_0):
		print("invalido")
	elif(winner_x):
		print("primeiro_venceu")
	elif(winner_0):
		print("segundo_venceu")
	elif(qt_x == qt_0 + 1 and qt_x < 5):
		print("segundo")
	elif(qt_x == qt_0):
		print("primeiro")
	else:
		print("empate")

	return

main()