#A heurística pensada para este problema foi baseada em
#analisar a vetor inserido par a par e verificando se
#algum par formaria a maior sequência de Fibonacci com os
#outros elementos.

def main():
  
  n = int(input())
  vec = map(int, raw_input().split())
  
  #Vetor auxiliar para inserir as sequências de Fibonnaci possíveis
  fib_list = [0]*n
  #Numero de ocorrências de um valor no vetor inserido
  num_occur = {}
  #Dicionário auxiliar para verificar se uma tupla já foi
  #verificada como termos iniciais da sequência de Fibonacci
  pair_occur = {}

  #Inicializando os dicionários de frequência
  for x in range(n):
    num_occur[vec[x]] = 0
  for x in range(n):
    num_occur[vec[x]] += 1

  for x in range(n):
    for y in range(n):
      pair_occur[(vec[x], vec[y])] = 0

  #Valor da maior sequência
  prefix_num = 2
  #Variável auxiliar para o vetor auxiliar "fib_list"
  i = 0

  for x in range(n):
    for y in range(n):
      if(x == y or pair_occur[(vec[x], vec[y])]):
        continue

      pair_occur[(vec[x], vec[y])] = 1
      prefix_num_aux = 2
      
      #Valores passados a outras variaveis para
      #evitar alteração no vetor original
      n1 = vec[x]
      n2 = vec[y]
      
      fib_list[i] = n1
      fib_list[i+1] = n2
      i += 2

      #Números adicionados no vetor auxiliar são
      #removidos logicamente 
      num_occur[n1] -= 1
      num_occur[n2] -= 1
        
      while((n1+n2) in num_occur and num_occur[n1+n2] > 0):
        num_occur[n1+n2] -= 1

        #Swap entre n1 e n2
        #pois n2 será o novo n1
        #e "n3"=(n1+n2) será o novo n2
        aux = n1
        n1 = n2
        n2 = aux

        n2 += n1
        prefix_num_aux += 1
        fib_list[i] = n2
        i += 1

        pair_occur[(n1, n2)] = 1

      prefix_num = max(prefix_num, prefix_num_aux)

      if(prefix_num == n):
        break

      #Correção da remoção lógica dos elementos
      for z in range(i):
        num_occur[fib_list[z]] += 1

      i = 0

    if(prefix_num == n):
      break

  return prefix_num

print(main())