import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import QuestionList from './QuestionList'
import CreateQuestion from './CreateQuestion'
import Header from './Header'


class App extends Component{
  render() {
    return(
      <div className="center w85">
        <Header />
        <div className="ph3 pv1 background-gray">
          <Switch>
            <Route exact path="/" component={QuestionList} />
            <Route exact path="/create" component={CreateQuestion} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default App