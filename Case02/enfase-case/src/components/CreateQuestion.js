import React, { Component } from 'react'
import { Mutation } from 'react-apollo'
import { QUESTION_LIST_QUERY } from './QuestionList'
import gql from 'graphql-tag'

const CREATE_QUESTION_MUTATION = gql`
  mutation CreateQuestionMutation(
    $description: String)
  {
    createQuestion(
      description: $description)
    {
      id
      description
    }
  }
`

class CreateQuestion extends Component {
  state = {
    description: '',
  }

  render() {
    const { description } = this.state
    return (
      <div>
        <div clasName="flex flex-column mt3">
          <input
            className="mb2"
            value={description}
            onChange={e => this.setState({ description: e.target.value })}
            type="text"
            placeholder="A description for the question"
          />
        </div>
        <Mutation
          mutation={CREATE_QUESTION_MUTATION}
          variables={{ description }}
          onCompleted={() => this.props.history.push('/')}
          update={(store, { data: { createQuestion } }) => {
            const data = store.readQuery({ query: QUESTION_LIST_QUERY })
            data.questionList.unshift(createQuestion)
            store.writeQuery({
              query: QUESTION_LIST_QUERY,
              data
            })
          }}
        >
          {createQuestionMutation =>
            <button
              onClick={createQuestionMutation}
            >
              Submit
            </button>}
        </Mutation>
      </div>
    )
  }
}

export default CreateQuestion