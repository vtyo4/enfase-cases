import React, { Component } from 'react'

class Question extends Component {
  render() {
    return (
      <div className="flex mt2 items-start">
        <div className="ml1">
          <div>
            {this.props.question.description} 
          </div>
        </div>
      </div>
    )
  }
}

export default Question