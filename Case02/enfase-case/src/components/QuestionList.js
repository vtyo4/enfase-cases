import React, { Component } from 'react'
import Question from './Question'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'


export const QUESTION_LIST_QUERY = gql`
  {
    questionList{
      id
      description
    }
  }
`

class QuestionList extends Component {
  render() {
    const questionListToRender = [
      {
        id: '1',
        description: 'Exercitation magna.',
      },
      {
        id: '2',
        description: 'Ut ea commodo in pariatur.',
      },
    ]

    return (
      <Query query={QUESTION_LIST_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>

          const questionListToRender = data.questionList

          return (
            <div>
              {questionListToRender.map((question, index) => (
                <Question
                  key={question.id}
                  question={question}
                  index={index}
                />
              ))}
            </div>
          )
        }}
      </Query>
    )
  }
}

export default QuestionList
