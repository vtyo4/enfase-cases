function questionList(root, args, context, info) {
  return context.prisma.questionList()
}
function answerList(root, args, context, info) {
  return context.prisma.answerList()
}


module.exports = {
 questionList,
 answerList,
}