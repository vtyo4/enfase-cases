function createQuestion(parent, args, context, info) {
  return context.prisma.createQuestion({
    description: args.description,
  })
}

module.exports = {
  createQuestion,
}