"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Answer",
    embedded: false
  },
  {
    name: "Question",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `https://eu1.prisma.sh/victor-antonio-de-oliveira-60a179/enfase-service/enfase-stage`
});
exports.prisma = new exports.Prisma();
